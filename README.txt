
-- SUMMARY --

This module adds new 'face detection' image style effects.

One will attempt to crop & centre a face in full resolution, the other will
crop, centre & scale to maximum x-y values.

You may also specify a pixel gutter surrounding a cropped face.

The hard part of this module is taken care of by the php-facedetection library
	(https://github.com/mauricesvay/php-facedetection)

For a full description of the module, visit the project page:
  http://drupal.org/project/image_face_detect

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/image_face_detect


-- REQUIREMENTS --

image, php-gd


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* No configuration for this module


-- CUSTOMIZATION --

* Create a new image style to take use of the new effects

  1) Visit configuration > media > image styles

  2) Add either the 'Centre on face' or 'Centre on face and scale' image effects

     - The X/Y buffer zone is the number of pixels that will be added to the	
       bounding box surrounding a face.


-- CONTACT --

Current maintainers:
* Stuart Rowlands - stuart@firecannon.com
