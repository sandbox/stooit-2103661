<?php

/**
 * @file
 * @author Stuart Rowlands <stuart@firecannon.com>
 */

/**
 * Implementation of hook_image_effect_info().
 */
function image_face_detect_image_effect_info() {
  $effects = array(
    'image_face_detect_centre_scale_face' => array(
      'label' => t('Centre on face and scale'),
      'help' => t('Face centred & scaled with pixel-edge buffer surroundings.'),
      'effect callback' => 'image_face_detect_centre_scale_face_effect',
      'form callback' => 'image_face_detect_centre_scale_face_form',
    ),
    'image_face_detect_centre_face' => array(
      'label' => t('Centre on face'),
      'help' => t('Face centred with pixel-edge buffer surroundings.'),
      'effect callback' => 'image_face_detect_centre_face_effect',
      'form callback' => 'image_face_detect_centre_face_form',
    ),
  );

  return $effects;
}

/**
 * Implementation of hook_theme().
 */
function image_face_detect_theme() {
  $theme = array(
    'image_face_detect_centre_scale_face_summary' => array(
      'variables' => array('data' => NULL),
    ),
    'image_face_detect_centre_face_summary' => array(
      'variables' => array('data' => NULL),
    ),
  );

  return $theme;
}


/**
 * Centre face, scale and crop.
 */
function image_face_detect_centre_scale_face_form($data = array()) {
  $form = image_resize_form($data);

  $form['bufferzone-y'] = array(
    '#type' => 'textfield',
    '#title' => t('Y Buffer zone (pixels above face)'),
    '#default_value' => isset($data['bufferzone-y']) ? $data['bufferzone-y'] : '100',
  );

  $form['bufferzone-x'] = array(
    '#type' => 'textfield',
    '#title' => t('X Buffer zone (pixels left of face)'),
    '#default_value' => isset($data['bufferzone-x']) ? $data['bufferzone-x'] : '100',
  );

  return $form;
}

/**
 * Centre face
 */

function image_face_detect_centre_face_form($data = array()) {

  $form['bufferzone-y'] = array(
    '#type' => 'textfield',
    '#title' => t('Y Buffer zone (pixels above face)'),
    '#default_value' => isset($data['bufferzone-y']) ? $data['bufferzone-y'] : '100',
  );

  $form['bufferzone-x'] = array(
    '#type' => 'textfield',
    '#title' => t('X Buffer zone (pixels left of face)'),
    '#default_value' => isset($data['bufferzone-x']) ? $data['bufferzone-x'] : '100',
  );

  return $form;
}


function image_face_detect_centre_face_effect(&$image, $data) {

  $detector = new svay\FaceDetector(dirname(__FILE__) .'/php-facedetection/detection.dat');
  $detector->faceDetect($image->resource);
  $face = $detector->getFace();
  if($face) {
        // update $data offset values
        $x = $face['x'] - ($data['bufferzone-x']/2);
        $y = $face['y'] - ($data['bufferzone-y']/2);

        $faceData['width'] = $face['w'] + $data['bufferzone-x'];
        $faceData['height'] = $face['w'] + $data['bufferzone-y'];

        $faceData['anchor'] = "$x-$y";
        return image_crop_effect($image, $faceData);
  } 

}


function image_face_detect_centre_scale_face_effect(&$image, $data) {

  $detector = new svay\FaceDetector(dirname(__FILE__) .'/php-facedetection/detection.dat');
  $detector->faceDetect($image->resource);
  $face = $detector->getFace();
  if($face) {
	// update $data offset values
	$x = $face['x'] - ($data['bufferzone-x']/2);
	$y = $face['y'] - ($data['bufferzone-y']/2);

	$faceData['width'] = $face['w'] + $data['bufferzone-x'];
	$faceData['height'] = $face['w'] + $data['bufferzone-y'];

	$faceData['anchor'] = "$x-$y";
	image_crop_effect($image, $faceData);
  }

  return image_scale_and_crop_effect($image, $data);

}
